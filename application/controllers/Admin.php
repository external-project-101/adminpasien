<?php
defined('BASEPATH') or exit('No direct script access allowed');
use GuzzleHttp\Client;

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        is_logged_in();
        $this->load->library('Pdf');
        
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }
    

    public function listPasien(){
        $data['title'] = 'Data Pasien';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        try {
            $client = new Client([
                'base_uri' => 'http://localhost:3000', 'timeout' => 5,
            ]);

            $response = $client->request('GET', '/pasien/list');
            $body = $response->getBody();
            $data['produk'] = json_decode($body)->data;
        }catch (Exception $e){
            $data['produk'] = null;
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/listdata', $data);
        $this->load->view('templates/footer');
    }

    public function submit(){
        $nama_anak = $this->input->post('nama_anak');
        $jenis_kelamin = $this->input->post('jeniskelamin');
        $nama_ibu = $this->input->post('nama_ibu');
        $tanggal_lahir = $this->input->post('tanggal_lahir');

        $data = array(
            'nama_anak' => $nama_anak, 'jenis_kelamin' => $jenis_kelamin, 'nama_ibu' => $nama_ibu, 'tgl_lahir' => $tanggal_lahir
        );

        try {
            $client = new Client([
                'base_uri' => 'http://localhost:3000', 'timeout' => 5,
            ]);

            $response = $client->request('POST', '/pasien/add',['json' => $data]);
            $body = $response->getBody();

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pasien telah ditambahkan</div>');
            redirect('admin/listpasien');
        }catch (Exception $e){
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Pasien gagal ditambahkan</div>');
        }
    }

    public function edit($id_produk){
        $data['title'] = 'Edit Data';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['id'] = $id_produk;
        try {
            $client = new Client([
                'base_uri' => 'http://localhost:3000', 'timeout' => 5,
            ]);

            $response = $client->request('GET', '/pasien/detail',['query'=>['doc_id' => $id_produk]]);
            $body = $response->getBody();
            $data['pasien'] = json_decode($body)->data;
        }catch (Exception $e){
            $data['pasien'] = null;
        }

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/pasien_edit', $data);
        $this->load->view('templates/footer');
    }

    public function update()
    {
        $id_pasien = $this->input->post('id_pasien');
        $nama_anak = $this->input->post('nama_anak');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $nama_ibu = $this->input->post('nama_ibu');
        $tanggal_lahir = $this->input->post('tanggal_lahir');

        $data = array('doc_id' => $id_pasien, 'nama_anak' => $nama_anak, 'jenis_kelamin' => $jenis_kelamin,
            'nama_ibu' => $nama_ibu, 'tgl_lahir' => $tanggal_lahir );

        try {
            $client = new Client([
                'base_uri' => 'http://localhost:3000', 'timeout' => 5,
            ]);

            $response = $client->request('POST', '/pasien/update',['json' => $data]);
            $body = $response->getBody();

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pasien telah diubah</div>');
            redirect('admin/listpasien');
        }catch (Exception $e){
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Pasien gagal diubah</div>');
        }
    }
	
	public function gizi()
	{
		$data['title'] = 'Pengecekan Gizi';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['pasien'] = null;
        #GET DATA FROM SERVER
		if(isset($_POST['keyword'])){
            try {
                $client = new Client([
                    'base_uri' => 'http://localhost:3000', 'timeout' => 5,
                ]);

                $response = $client->request('GET', '/gizi/checkgizi',['query'=>['name' => $_POST['keyword']]]);
                $body = $response->getBody();
                $data['pasien'] = json_decode($body)->data;
            }catch (Exception $e){
                $data['pasien'] = null;
            }
        }
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/listgizi', $data);
        $this->load->view('templates/footer');
	}

	public function updategizi()
    {
        $nama_anak = $_POST['nama_anak'];
        $berat = $_POST['berat'];
        $tinggi = $_POST['tinggi'];
        $usia = $_POST['usia'];
        $status_gizi = $_POST['status_gizi'];
        $imt = $_POST['imt'];
        $tanggal_pengecekan = $_POST['tanggal'];
        #GET HITUNG FROM SERVER
        if(isset($_POST['hitung'])){
            try {
                $client = new Client([
                    'base_uri' => 'http://localhost:3000', 'timeout' => 5,
                ]);

                $response = $client->request('POST', '/gizi/hitung',['json'=>[]]);
                $body = $response->getBody();
                $data['pasien'] = json_decode($body)->data;
            }catch (Exception $e){
                $data['pasien'] = null;
            }
        }

        #UNTUK SIMPAN DATA GIZI KE SERVER
        if(isset($_POST['simpan'])){
            $data = array('nama_anak' => $nama_anak, 'berat_badan' => $berat, 'tinggi_badan' => $tinggi, 'usia' => $usia,
                'tanggal_check' => $tanggal_pengecekan, 'imt' => $imt, 'status_gizi' => $status_gizi);
            try {
                $client = new Client([
                    'base_uri' => 'http://localhost:3000', 'timeout' => 5,
                ]);

                $response = $client->request('POST', '/gizi/update',['json'=> $data ]);
                $body = $response->getBody();
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Gizi Pasien telah ditambahkan </div>');
                redirect('admin/gizi');
            }catch (Exception $e){
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Gizi Pasien gagal ditambahkan</div>');
            }
        }
    }

	public function validasi()
	{
		$data['title'] = 'Validasi Data';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/listvalidasi', $data);
        $this->load->view('templates/footer');
	}
	
	public function training()
	{
		$data['title'] = 'Data Training';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/training', $data);
        $this->load->view('templates/footer');
	}
	
	public function testing()
	{
		$data['title'] = 'Data Testing';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		
		$this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/testing', $data);
        $this->load->view('templates/footer');
	}
}
