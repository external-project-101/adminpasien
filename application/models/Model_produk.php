<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_produk extends CI_Model
{
    public function tampil_produk(){
        $this->db->select("tbl_produk.*, tbl_kategori.kategori" )
         ->from("tbl_produk")
         ->join("tbl_kategori", "tbl_kategori.id_kategori = tbl_produk.id_kategori");
        return $result = $this->db->get()->result_array();
    }

    public function input_data($data,$table){
        $this->db->insert($table,$data);
    }

    public function edit($where,$table){
        return $this->db->get_where($table, $where);
    }

    public function delete($id_produk){
        $this->db->where('id_produk', $id_produk);
        return $this->db->delete('tbl_produk');
    }

    public function getDataByID($id_produk){
        return $this->db->get_where('tbl_produk', array('id_produk'=>$id_produk));
    }

    public function find($id_produk){
        $result = $this->db->where('id_produk', $id_produk)->limit(1)->get('tbl_produk');
        if($result->num_rows() > 0){
            return $result->row();

        }else{
            return array();
        }
    }

    public function detail_produk($id_produk){
        $result = $this->db->where('id_produk', $id_produk)->get('tbl_produk');
        if($result->num_rows() > 0){
            return $result->result();
        }else {
            return false;
        }
    }

    public function update_data($data,$table,$id){
        $where = ['id_produk' => $id];
        $this->db->update($table,$data,$where);
    }

    public function countAll(){
        $result = $this->db->get('tbl_produk');
        return $result->num_rows();
    }


}

?>