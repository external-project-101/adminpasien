<?php

class Model_category extends CI_Model{
    public function kategori_1(){
        return $this->db->get_where('tbl_produk', array('id_kategori' => '1'));
    }

    public function kategori_2(){
        return $this->db->get_where('tbl_produk', array('id_kategori' => '2'));
    }

    public function kategori_3(){
        return $this->db->get_where('tbl_produk', array('id_kategori' => '3'));
    }

    public function kategori_4(){
        return $this->db->get_where('tbl_produk', array('id_kategori' => '4'));
    }

    public function list_kategori()
    {
        $this->db->order_by('id_kategori');
        $query = $this->db->get('tbl_kategori');

        return $query->result_array();
    }
}




?>