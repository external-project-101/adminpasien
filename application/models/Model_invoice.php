<?php

class Model_invoice extends CI_Model{
    public function index(){
        date_default_timezone_set('Asia/Jakarta');
        $total_items = $total_belanja = 0;
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $jasa = $this->input->post('jasa');
        $bank = $this->input->post('bank');

        $invoice = array (
            'nama' => $nama, 'alamat' => $alamat, 'nama_bank' => $bank, 'nama_jasa' => $jasa,
            'status' => 'unpaid', 'tgl_pesan' => date('Y-m-d  H:i:s'),
            'batas_bayar' => date('Y-m-d  H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 2, date('Y')))
        );

        $this->db->insert('tbl_invoice', $invoice);
        $id_invoice = $this->db->insert_id();

        foreach ($this->cart->contents() as $item){
            #UNTUK SAVE TO PESANAN
            $data = array (
                'id_invoice' =>$id_invoice, 'id_produk' =>$item['id'],
                'nama' =>$item['name'], 'jumlah' =>$item['qty'], 'harga' =>$item['price'],
            );
            $this->db->insert('tbl_pesanan', $data);

            $total_items = $total_items + $item['qty'];
            $total_belanja = $total_belanja + ($item['qty'] * $item['price']);
        }

        $this->db->update('tbl_invoice', ['total_items' => $total_items, 'total_belanja' => $total_belanja],
            ['id_invoice' => $id_invoice ]);

        return TRUE;
    }

    public function tampil_data(){
        $result = $this->db->get('tbl_invoice');
        if($result->num_rows() > 0){
            return $result->result();
        }else {
            return false;
        }
    }

    public function ambil_id_invoice($id_invoice){
        $result = $this->db->where('id_invoice', $id_invoice)->limit(1)->get('tbl_invoice');

        if($result->num_rows() > 0){
            return $result->row();
        }else {
            return false;
        }
    }

    public function ambil_id_pesanan($id_invoice){
        $result = $this->db->where('id_invoice',$id_invoice)->get('tbl_pesanan');

        if($result->num_rows() > 0){
            return $result->result();
        }else {
            return false;
        }
    }

    public function countAllBuyers(){
        $result = $this->db->get('tbl_invoice');
        return $result->num_rows();
    }

    public function getDataPerBulan()
    {
        return $this->db->select('SUM(tbl_pesanan.harga * tbl_pesanan.jumlah) AS harga, month(tbl_invoice.batas_bayar) as bulan')
            ->join('tbl_invoice', 'tbl_invoice.id_invoice = tbl_pesanan.id_invoice')
            ->group_by('MONTH(tbl_invoice.batas_bayar)')
            ->where('YEAR(tbl_invoice.batas_bayar)', date('Y'))
            ->order_by('bulan', 'asc')
            ->get('tbl_pesanan')
            ->result();
    }

    public function user_invoice($user_id){
        $result = $this->db->where('id_user', $user_id)->get('tbl_invoice');
        if($result->num_rows() > 0){
            return $result->result();
        }else {
            return false;
        }
    }

    public function payment(){
        date_default_timezone_set('Asia/Jakarta');
        $id_invoice = $this->input->post('id_invoice');
        $no_rekening = $this->input->post('no_rekening');
        $nama_pemilik = $this->input->post('nama_pemilik');
        $jumlah_transfer = $this->input->post('jumlah_transfer');
        $bukti_transfer = $this->input->post('bukti_transfer');
        $tanggal_bayar = $this->input->post('tgl_bayar');

        $invoice = array (
                'nama_pemilik' => $nama_pemilik, 'jumlah_transfer' => $jumlah_transfer, 'bukti_bayar' => $bukti_transfer,
                'tgl_bayar' => $tanggal_bayar, 'status' => 'paid', 'no_rekening' => $no_rekening
            );
        $this->db->update('tbl_invoice', $invoice, ['id_invoice' => $id_invoice]);

        return TRUE;
    }
}

?>